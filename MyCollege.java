import college.College;

class MyCollege extends College {
    int numberOfStudent;
    Boolean isCollegeActive;

    MyCollege(String nameOfCollege, String locationOfCollege) {
        super(nameOfCollege, locationOfCollege);
        this.numberOfStudent = numberOfStudent != 0 ? numberOfStudent : 0;
        this.isCollegeActive = isCollegeActive != null ? isCollegeActive : true;
    }

    MyCollege(String nameOfCollege, String locationOfCollege, int numberOfStudent, Boolean isCollegeActive) {
        super(nameOfCollege, locationOfCollege);
        this.numberOfStudent = numberOfStudent != 0 ? numberOfStudent : 0;
        this.isCollegeActive = isCollegeActive != null ? isCollegeActive : true;
    }

    // Getter
    public int getNumberOfStudent() {
        return numberOfStudent;
    }

    // Setter
    public void setNumberOfStudent(int numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }

    // Getter
    public Boolean getIsCollegeActive() {
        return isCollegeActive;
    }

    // Setter
    public void setIsCollegeActive(Boolean isCollegeActive) {
        this.isCollegeActive = isCollegeActive;
    }

    public void setCollegeStatus(Boolean isCollegeActive) {
        this.isCollegeActive = isCollegeActive;
    }

    public void addStudent(int n) {
        this.numberOfStudent += n;
    }
}

class Debug {
    public static void main(String[] args) {

        MyCollege myCollege = new MyCollege("RVR", "Guntur");
        System.out.println(myCollege.getLocationOfCollege());

        System.out.println(myCollege.getNameOfCollege());

        System.out.println(myCollege.getNumberOfStudent());

        System.out.println(myCollege.getIsCollegeActive());

        myCollege.addStudent(5);

        System.out.println(myCollege.getNumberOfStudent());

        myCollege.setCollegeStatus(false);

        System.out.println(myCollege.getIsCollegeActive());

    }
}