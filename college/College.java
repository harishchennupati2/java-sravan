package college;

public class College {
    private String nameOfCollege;

    private String locationOfCollege;

    public College(String nameOfCollege,String locationOfCollege){
        this.nameOfCollege = nameOfCollege;
        this.locationOfCollege = locationOfCollege;
    }

    // Getter
    public String getNameOfCollege() {
        return nameOfCollege;
    }

    // Setter
    public void setNameOfCollege(String c) {
        this.nameOfCollege = c;
    }

    public String getLocationOfCollege() {
        return locationOfCollege;
    }

    // Setter
    public void setLocationOfCollege(String c) {
        this.locationOfCollege = c;
    }
}


